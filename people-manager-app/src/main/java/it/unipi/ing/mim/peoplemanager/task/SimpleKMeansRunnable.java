package it.unipi.ing.mim.peoplemanager.task;

import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.deep.tools.Output;
import it.unipi.ing.mim.peoplemanager.ui.FXMLDocumentController;
import it.unipi.ing.mim.peoplemanager.ui.db.UnknownPeopleMongoDB;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import weka.clusterers.SimpleKMeans;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class SimpleKMeansRunnable extends Thread {

    private static Random RANDOM = new Random();
    private List<ImgDescriptor> imgDescriptors = null;
    private List<ImgDescriptor> newPeopleImgDesc = null;
    private UnknownPeopleMongoDB unknownPeopleMongo = new UnknownPeopleMongoDB();
    public int previousClusterNumber = -1;
    
    private ListView notificationList;

    public SimpleKMeansRunnable( ListView notifList ) {
        notificationList = notifList;
    }

    @Override
    public void run() {
        try {
            System.out.println("---- Clustering thread started!");
            System.out.println("---- Previous cluster number: " + previousClusterNumber);
            /* loading imgDescriptor from file */
            if( previousClusterNumber == -1 ) {//First time
                imgDescriptors = unknownPeopleMongo.listAllUnknownPeople();
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.add( Parameters.CALENDAR_TIME_TYPE, Parameters.CALENDAR_TIME_AMOUNT );

                boolean areThereNewRecords = unknownPeopleMongo.areThereNewRecords( calendar.getTime() );
                if( areThereNewRecords ) {
                    imgDescriptors = unknownPeopleMongo.listAllUnknownPeopleFrom( calendar.getTime() );
                }
            }
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
        if( ! imgDescriptors.isEmpty() ) {
            int[] assignments = null;
            try {

                /* creating instances object */
                Instances instances = buildDataSet(imgDescriptors);
                /* aprox number of clusters */

                int numberOfClusters = (int) (Math.ceil(Math.sqrt(imgDescriptors.size() / 2.0)));

                SimpleKMeans kmeans = new SimpleKMeans();
                kmeans.setSeed(RANDOM.nextInt());

                kmeans.setPreserveInstancesOrder(true);
                kmeans.setNumClusters(numberOfClusters);
                kmeans.buildClusterer(instances);
                assignments = kmeans.getAssignments();
            } catch (Exception e) {
                //e.printStackTrace();
            }

            Map<Integer, List<ImgDescriptor>> clusterImgDescriptors = new HashMap<Integer, List<ImgDescriptor>>();
            int clusterNumber = 0;
            for (int i = 0; i < assignments.length; i++) {
                clusterNumber = assignments[i];
                ImgDescriptor imgDescriptor = imgDescriptors.get(i);
                imgDescriptor.setClusterNumber(clusterNumber);
                System.out.printf("Instance %d -> Cluster %d\n", i, clusterNumber);

                if (!clusterImgDescriptors.containsKey(clusterNumber)) {
                    clusterImgDescriptors.put(clusterNumber, new ArrayList<>());
                }
                clusterImgDescriptors.get(clusterNumber).add(imgDescriptor);
            }

            List<ImgDescriptor> toPrint = new ArrayList<>();
            for (Entry<Integer, List<ImgDescriptor>> entry : clusterImgDescriptors.entrySet()) {
                toPrint.addAll(entry.getValue());
            }

            Collections.sort(toPrint, new Comparator<ImgDescriptor>() {
                @Override
                public int compare(ImgDescriptor o1, ImgDescriptor o2) {
                    return o1.getClusterNumber().compareTo(o2.getClusterNumber());
                }
            });

            Output.toHTML(toPrint, Parameters.BASE_URI, Parameters.RESULTS_HTML);

            //Emit a new alert for each new person
            if( ! ( previousClusterNumber == -1 ) ) {//It is no the first time

                if( clusterImgDescriptors.size() > previousClusterNumber ) {
                    int newAlertNumber = clusterImgDescriptors.size() - previousClusterNumber;
                    System.out.println("The number of clusters is changed! It will emit " + newAlertNumber );

                    Date date = new Date();
                    for( int i = 0; i < newAlertNumber; i++ ){
                        String pattern = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                        
                        //Update the UI
                        Platform.runLater(new Runnable() {
                            @Override public void run() {
                                notificationList.getItems().add( Parameters.ALERT_LIST_MESSAGE + simpleDateFormat.format(date) );
                            }
                        });
                        

                    }
                }
            }
            
            System.out.println("---- Current cluster number: " + clusterImgDescriptors.size() );
            previousClusterNumber = clusterImgDescriptors.size();
            
            FXMLDocumentController.clusteredImgDescriptor = toPrint;
            
        }//end if 
        else {
            System.out.println("---- No images" );
            previousClusterNumber = 0;
        }

        System.out.println("---- Clear all img descriptors in thread" );
        imgDescriptors.clear();
    }

    private Instances buildDataSet(List<ImgDescriptor> imgDescriptors) {
        int numberFeatures = Parameters.FEATURE_NUMBER;
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        // features
        for (int i = 0; i < numberFeatures; i++) {
            attributes.add(new Attribute("f_" + i));
        }
        /* lets construct our object with an initial capacity of 10 elements */
        int initialCapacity = Parameters.K;
        Instances instances = new Instances("unknown_data", attributes, initialCapacity);
        List<ImgDescriptor> copyList = new ArrayList<>( imgDescriptors );
        for (ImgDescriptor descriptor : copyList) {
            Instance instance = new DenseInstance(numberFeatures);
            for (int i = 0; i < descriptor.getFeatures().length; i++) {
                instance.setValue(instances.attribute(i), descriptor.getFeatures()[i]);
            }
            instances.add(instance);
        }
        return instances;
    }
}
