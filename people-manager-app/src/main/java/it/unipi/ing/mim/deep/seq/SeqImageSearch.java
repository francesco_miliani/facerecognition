package it.unipi.ing.mim.deep.seq;

import it.unipi.ing.mim.deep.DNNExtractor;
import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.deep.tools.Output;
import it.unipi.ing.mim.peoplemanager.ui.db.AuthorizedPeopleMongoDB;
import it.unipi.ing.mim.peoplemanager.ui.db.UnknownPeopleMongoDB;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SeqImageSearch {

    private List<ImgDescriptor> descriptors;
    private Map<String, List<ImgDescriptor>> authorizedUserMap;
    private AuthorizedPeopleMongoDB authorizedPeopleMongoDB;

    private static final SeqImageSearch instance = new SeqImageSearch();

    public static SeqImageSearch getInstance() {
        return instance;
    }

    public void addAuthorizedUser(Map<String, List<ImgDescriptor>> authorizedUserMapParam) {
        synchronized (descriptors) {
            for (Map.Entry<String, List<ImgDescriptor>> userEntry : authorizedUserMapParam.entrySet()) {
                if (!authorizedUserMap.containsKey(userEntry.getKey())) {
                    authorizedUserMap.put(userEntry.getKey(), userEntry.getValue());
                    descriptors.addAll(userEntry.getValue());
                }
            }
        }
    }

    public void removeAuthorizedUsers(List<String> userList) {
        synchronized (descriptors) {
            for (String user : userList) {
                if (authorizedUserMap.containsKey(user)) {
                    List<ImgDescriptor> toRemove = authorizedUserMap.remove(user);
                    if (toRemove != null && !toRemove.isEmpty()) {
                        descriptors.removeAll(toRemove);
                    }
                }
            }
        }
    }

    private SeqImageSearch() {
        authorizedPeopleMongoDB = new AuthorizedPeopleMongoDB();
        authorizedUserMap = authorizedPeopleMongoDB.listAllAuthorizedPeople();
        descriptors = new ArrayList<ImgDescriptor>();
        if (!authorizedUserMap.isEmpty()) {
            for (Map.Entry<String, List<ImgDescriptor>> entry : authorizedUserMap.entrySet()) {
                descriptors.addAll(entry.getValue());
            }
        }
        descriptors = Collections.synchronizedList(descriptors);
    }

    public static void main(String[] args) throws Exception {

        SeqImageSearch searcher = new SeqImageSearch();

        //Image Query File
        File img = new File("/home/n3gr3t4/NetBeansProjects/FaceRecognition/facerecognition/face-recognition-monitor/data/classes/miliani", "0_miliani.jpg");

        DNNExtractor extractor = DNNExtractor.getInstance();

        float[] features = extractor.extract(img, Parameters.DEEP_LAYER);
        ImgDescriptor query = new ImgDescriptor(features, img.getName(), null);

        long time = -System.currentTimeMillis();
        List<ImgDescriptor> res = searcher.search(query, Parameters.K, true);//true stands for weighted
        time += System.currentTimeMillis();
        System.out.println("Sequential search time: " + time + " ms");

        Output.toHTML(res, Parameters.BASE_URI, Parameters.RESULTS_HTML);

    }

    public List<ImgDescriptor> search(ImgDescriptor queryF, int k, boolean weighted) {
        if (weighted) {
            return weightedKNN(queryF, k);
        } else {
            return knn(queryF, k);
        }
    }

    public List<ImgDescriptor> knn(ImgDescriptor queryF, int k) {
        synchronized (descriptors) {
            for (int i = 0; i < descriptors.size(); i++) {
                descriptors.get(i).distance(queryF);
            }
            Collections.sort(descriptors);
        }
        return descriptors.subList(0, k);
    }

    public List<ImgDescriptor> weightedKNN(ImgDescriptor queryF, int k) {
        synchronized (descriptors) {
            if (descriptors != null && !descriptors.isEmpty() && k > 0) {
                for (ImgDescriptor imgDescriptor : descriptors) {
                    //Compute the weight with respect to the query
                    imgDescriptor.computeWeight(queryF);
                }
                /* here we need to sort in descreasing order */
                Collections.sort(descriptors, new Comparator<ImgDescriptor>() {
                    @Override
                    public int compare(ImgDescriptor o1, ImgDescriptor o2) {
                        return new Double(o2.getWeight()).compareTo(o1.getWeight());
                    }
                });
                return descriptors.subList(0, k);
            }
        }
        return null;
    }

}
