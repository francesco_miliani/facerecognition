package it.unipi.ing.mim.opencv.facerecognition;

import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.deep.seq.SeqImageSearch;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class KNNClassifier {

    private SeqImageSearch sequentialScan;

    public KNNClassifier() throws IOException, ClassNotFoundException {
        sequentialScan = SeqImageSearch.getInstance();
    }

    //TODO
    /**
     * It takes the ImgDescriptor of the query (containing the CNN feature of
     * the face, i.e., the LightenedCNN_A model) It performs a kNN similarity
     * search (SeqImageSearch), and It calls getBestLabel to retrieve the best
     * label passing the result set
     *
     * @param query
     * @return
     */
    public PredictedClass predict(ImgDescriptor query) {
        //perform a kNN similarity search and call getBestLabel to retrieve the best label
        List<ImgDescriptor> results = sequentialScan.search(query, Parameters.K, true);
        PredictedClass predClass = getBestLabel(results);

        return predClass;
    }

    //TODO
    /**
     * It takes the result of the kNN query Perform a weighted sum It loops in
     * the results list and retrieve the best label Tip: use a java hashmap:
     * HashMap<String, Integer>
     * String -> label of the face Integer -> number of times It returns the
     * PredictedClass containing the label of the face and its confidence
     * (probability of recognition), given by k/n (k is the number of times of
     * the label)
     *
     * @param results
     * @return
     */
    private PredictedClass getBestLabel(List<ImgDescriptor> results) {
        //Loop in the results list and retrieve the best label
        Map<String, Double> labelMap = new HashMap<>();
        String maxLabel = "";
        double maxOccur = 0;
        double weight_sum = 0.0;
        for (ImgDescriptor img : results) {
            weight_sum += img.getWeight();//Summing all the weights
            if (!labelMap.containsKey(img.getLabel())) {
                labelMap.put(img.getLabel(), 1.0 * img.getWeight());
            } else {
                labelMap.put(img.getLabel(), labelMap.get(img.getLabel()) + img.getWeight());//Increments the value                
            }
        }

        for(Entry<String, Double> labelEntry:labelMap.entrySet()){
            if (labelEntry.getValue() > maxOccur){
                maxOccur = labelEntry.getValue();
                maxLabel = labelEntry.getKey();
            }
        }

        PredictedClass predClass = new PredictedClass(maxLabel, (float)(maxOccur / weight_sum));
        return predClass;
    }
    
}
