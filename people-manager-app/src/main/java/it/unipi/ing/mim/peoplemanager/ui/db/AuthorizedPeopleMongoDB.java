package it.unipi.ing.mim.peoplemanager.ui.db;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.client.model.Projections;
import it.unipi.ing.mim.deep.DNNExtractor;
import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bson.Document;

public class AuthorizedPeopleMongoDB {

    private static final String MONGODB_SERVER = "127.0.0.1";
    private static final Integer MONGODB_PORT = 27017;
    private static final String MONGODB_DATABASE = "facerecognition";
    private static final String AUTHORIZED_PEOPLE_COLLECTION = "authorized_people";

    private MongoClient connectToMongoDB() {
        return new MongoClient(MONGODB_SERVER, MONGODB_PORT);
    }

    public boolean isCollectionExists() {
        boolean exists = false;
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            exists = isCollectionExists(database);
        }
        return exists;
    }

    private boolean isCollectionExists(MongoDatabase database) {
        MongoIterable<String> collection = database.listCollectionNames();
        for (String s : collection) {
            if (s.equals(AUTHORIZED_PEOPLE_COLLECTION)) {
                return true;
            }
        }
        return false;
    }

    public void rebuildAuthorizedCollection() {
        // Delete All documents from collection Using blank BasicDBObject
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            if (isCollectionExists(database)) {
                database.getCollection(AUTHORIZED_PEOPLE_COLLECTION).drop();
            }
        }

        File srcFolder = Parameters.SRC_FOLDER;
        File[] classesFolder = srcFolder.listFiles();
        for (File classFolder : classesFolder) {
            if (classFolder.isDirectory()) {
                String folderName = classFolder.getName();
                File[] imgFiles = classFolder.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().endsWith(".jpg");
                    }
                });
                List<ImgDescriptor> features = new ArrayList<ImgDescriptor>();
                if (imgFiles != null && imgFiles.length == Parameters.MAX_SELECTABLE_PHOTOS) {
                    for (File imgFile : imgFiles) {
                        String imageName = imgFile.getName();
                        if (!imageName.startsWith(".")) {
                            try {
                                float[] featureArr = DNNExtractor.getInstance().extract(imgFile, Parameters.DEEP_LAYER);
                                features.add(new ImgDescriptor(featureArr, imgFile.getName(), imgFile.getParentFile().getName()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    createAuthorizedPerson(folderName, features);
                }
            }
        }
    }

    public void removeAuthorizedPerson(String userName) {
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            MongoCollection<Document> authorizedPeopleCollection = database.getCollection(AUTHORIZED_PEOPLE_COLLECTION);
            BasicDBObject removeDocument = new BasicDBObject();
            removeDocument.put("active", false);
            removeDocument.append("update_date", new Date());
            authorizedPeopleCollection.updateOne(Filters.and(Filters.eq("user", userName), Filters.eq("active", true)), new Document("$set", removeDocument));
        }
    }

    public void createAuthorizedPerson(String name, List<ImgDescriptor> descriptors) {
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            MongoCollection<Document> authorizedPeopleCollection = database.getCollection(AUTHORIZED_PEOPLE_COLLECTION);
            Document authorizedPerson = new Document("user", name);
            BasicDBList imagesList = new BasicDBList();
            int i = 0;
            for (ImgDescriptor desc : descriptors) {
                Document document = new Document();
                float[] features = desc.getFeatures();
                ArrayList<Float> featuresList = new ArrayList<>();
                for (float f : features) {
                    featuresList.add(f);
                }
                document.append("filename", desc.getId());
                document.append("features", featuresList);
                imagesList.add(document);
            }
            authorizedPerson.append("update_date", new Date());
            authorizedPerson.append("active", true);
            authorizedPerson.append("images", imagesList);
            authorizedPeopleCollection.insertOne(authorizedPerson);
        }
    }

    public Map<String, List<ImgDescriptor>> listAllAuthorizedPeople() {
        return buildAuthorizedUserMap(null);
    }

    public Map<String, List<ImgDescriptor>> listAuthorizedPeopleFrom(Date date) {
        return buildAuthorizedUserMap(date);
    }

    private Map<String, List<ImgDescriptor>> buildAuthorizedUserMap(Date date) {
        Map<String, List<ImgDescriptor>> authorizedPeopleMap;
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            MongoCollection<Document> authorizedPeopleCollection = database.getCollection(AUTHORIZED_PEOPLE_COLLECTION);
            BasicDBObject query = new BasicDBObject();
            if (date != null) {
                query.put("update_date", BasicDBObjectBuilder.start("$gte", date).get());
            }
            query.put("active", true);
            FindIterable<Document> cursor = authorizedPeopleCollection.find(query);
            authorizedPeopleMap = new HashMap<String, List<ImgDescriptor>>();
            Iterator<Document> it = cursor.iterator();
            while (it.hasNext()) {
                Document currentDocument = it.next();
                List<ImgDescriptor> listDesc = new ArrayList<ImgDescriptor>();
                String userName = currentDocument.getString("user");
                ArrayList<Document> featureList = (ArrayList<Document>) currentDocument.get("images");
                featureList.forEach((d) -> {
                    String imageFileName = d.getString("filename");
                    ArrayList<Double> features = (ArrayList<Double>) d.get("features");
                    float[] desc_features = new float[features.size()];
                    for (int i = 0; i < features.size(); i++) {
                        desc_features[i] = features.get(i).floatValue();
                    }
                    ImgDescriptor desc = new ImgDescriptor(desc_features, imageFileName, userName);
                    listDesc.add(desc);
                });
                authorizedPeopleMap.put(userName, listDesc);
            }
        }
        return authorizedPeopleMap;
    }

    public List<String> listAuthorizedPeopleName() {
        return listPeople(true, null);
    }

    public List<String> listDeletedAuthorizedPeople(Date from) {
        return listPeople(false, from);
    }

    private List<String> listPeople(boolean active, Date date) {
        List<String> names = new ArrayList<String>();
        MongoClient mongoClient = connectToMongoDB();
        MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
        MongoCollection<Document> authorizedPeopleCollection = database.getCollection(AUTHORIZED_PEOPLE_COLLECTION);
        BasicDBObject query = new BasicDBObject();
        if (date != null) {
            query.put("update_date", BasicDBObjectBuilder.start("$gte", date).get());
        }
        query.put("active", active);
        FindIterable<Document> cursor = authorizedPeopleCollection.find(query).projection(Projections.include("user"));
        Iterator<Document> it = cursor.iterator();
        while (it.hasNext()) {
            Document dbObject = it.next();
            names.add(dbObject.getString("user"));
        }
        mongoClient.close();
        return names;
    }

    public boolean existsUser(String userName) {
        long count = 0;
        MongoClient mongoClient = connectToMongoDB();
        BasicDBObject query = new BasicDBObject();
        query.put("user", userName);
        query.put("active", true);
        count = mongoClient.getDatabase(MONGODB_DATABASE)
                .getCollection(AUTHORIZED_PEOPLE_COLLECTION)
                .count(query);
        mongoClient.close();
        return count > 0;
    }

    public static void main(String[] args) {
        AuthorizedPeopleMongoDB authorizedPeopleMongoDB = new AuthorizedPeopleMongoDB();
        authorizedPeopleMongoDB.rebuildAuthorizedCollection();
        Map<String, List<ImgDescriptor>> userDescriptorMap = authorizedPeopleMongoDB.listAllAuthorizedPeople();
        for (Entry<String, List<ImgDescriptor>> entry : userDescriptorMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue().size());
        }
        // since this date is recent and there is no new date, it is expected 0 rows.
        userDescriptorMap = authorizedPeopleMongoDB.listAuthorizedPeopleFrom(new Date());
        System.out.println("descriptors " + userDescriptorMap.size());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -1);
        // since this date is ONE hour ago, it is expected to get many rows.
        userDescriptorMap = authorizedPeopleMongoDB.listAuthorizedPeopleFrom(calendar.getTime());
        System.out.println("descriptors " + userDescriptorMap.size());

        List<String> users = authorizedPeopleMongoDB.listAuthorizedPeopleName();
        for (String user : users) {
            System.out.println(user);
        }
        System.out.println("Exist user chicho?: " + authorizedPeopleMongoDB.existsUser("chicho"));
        System.out.println("Exist user andrea?: " + authorizedPeopleMongoDB.existsUser("andrea"));
    }
}
