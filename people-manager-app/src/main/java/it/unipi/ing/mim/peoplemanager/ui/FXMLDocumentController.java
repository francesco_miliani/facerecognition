package it.unipi.ing.mim.peoplemanager.ui;

import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.opencv.facerecognition.KNNClassifier;
import it.unipi.ing.mim.opencv.facerecognition.PredictedClass;
import it.unipi.ing.mim.peoplemanager.task.SimpleKMeansRunnable;
import it.unipi.ing.mim.peoplemanager.ui.db.UnknownPeopleMongoDB;
import it.unipi.ing.mim.peoplemanager.ui.db.AuthorizedPeopleMongoDB;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

public class FXMLDocumentController implements Initializable {

    @FXML
    private Button pressedButton, addButton, deleteButton, clearButton, clearSelectionButton;
    @FXML
    private GridPane photoPane;
    @FXML
    private ListView notificationList, authorizedPeopleList;
    @FXML
    private TextField nameTextField;
    @FXML
    private Label personLabel;

    private final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> scheduleAtFixedRate = null;
    private Runnable clusteringRunnable;
    private final UnknownPeopleMongoDB unknownPeopleMongoDB = new UnknownPeopleMongoDB();
    private final AuthorizedPeopleMongoDB authorizedPeopleMongoDB = new AuthorizedPeopleMongoDB();
    
    public static List<ImgDescriptor> selectedItemList = new ArrayList<>();
    public static List<String> authorizedPeople = new ArrayList<>();
    public static List<Pane> selectedPaneList = new ArrayList<>();
    public static ImgDescriptor[][] imgDescMatrix = null;//new ImgDescriptor[10][Parameters.GRID_COLUMN_NUMBER];
    private boolean itemSelected = false;

    public static List<ImgDescriptor> clusteredImgDescriptor;
    private int selectedItemIndex = -1;
    String personToBeAuthorizedName;


    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Button: " + event.getSource());
        pressedButton = (Button) event.getSource();
        if (pressedButton.getId().equals("addButton")) {
            System.out.println("Pressed add button");
            handleAddButton();
        } else if (pressedButton.getId().equals("deleteButton")) {
            System.out.println("Pressed delete button");
            handleDeleteButton();
        } else if (pressedButton.getId().equals("clearButton")) {
            handleClearButton();
            System.out.println("Pressed clear button");
        } else if (pressedButton.getId().equals("removeButton")) {
            handleRemoveButton();
            System.out.println("Pressed remove button");
        }  else if (pressedButton.getId().equals("deleteAllUnknownButton")) {
            handleDeleteAllUnknownButton();
            System.out.println("Pressed delete All Unknown Button button");
        } else {
            handleClearSelectionButton();
            System.out.println("Pressed clear selection button");
        }

    }

    /**
     * Set the notificationList & authorizedPeopleList handle
     * Start a periodic thread for clustering purpose
     * Hide or show parts of UI
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    private void init() throws IOException, ClassNotFoundException {
        System.out.println("Init");
        clusteringRunnable = (Runnable) new SimpleKMeansRunnable( notificationList );
        scheduleAtFixedRate = scheduler.scheduleAtFixedRate(clusteringRunnable, 0, Parameters.PERIODIC_TIME_AMOUNT, Parameters.PERIODIC_TIME_TYPE);
        System.out.println("----Init: clustering thread started");

        authorizedPeople = authorizedPeopleMongoDB.listAuthorizedPeopleName();
        for( String name : authorizedPeople)
           authorizedPeopleList.getItems().add( name );//List view
        
        //TESTING
        //notificationList.getItems().add( Parameters.ALERT_LIST_MESSAGE + "16:00:00 of 2018/01/01" );
        //notificationList.getItems().add( Parameters.ALERT_LIST_MESSAGE + "16:00:00 of 2018/01/01" );
        //notificationList.getItems().add( Parameters.ALERT_LIST_MESSAGE + "16:00:00 of 2018/01/01" );
              
        notificationList.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if( notificationList.getItems().isEmpty() ) {
                    hideAddPersonPane();
                    return;
                }

                nameTextField.setDisable(false);
                ObservableList selectedIndex = notificationList.getSelectionModel().getSelectedIndices();
                System.out.println("selectedIndex: clicked on " + selectedIndex);
                if( selectedIndex.isEmpty() ) {
                    System.out.println("Clicked on no item");
                    selectedItemIndex = -1;
                    return;
                }
        
                selectedItemIndex = (int)selectedIndex.get(0);
                System.out.println("selectedItemIndex: clicked on " + selectedItemIndex);
                itemSelected = true;
                showAddPersonPane();
                
                emitAlert(Parameters.AUTHORIZATION_ALERT_MESSAGE);
            }
        });

        //This is important! NOT FOR TESTING
        authorizedPeopleList.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

                ObservableList selectedIndex = authorizedPeopleList.getSelectionModel().getSelectedIndices();
                if( selectedIndex.equals(-1) ) {
                    selectedItemIndex = -1;
                    return;
                }
                selectedItemIndex = (int)selectedIndex.get(0);
                System.out.println("clicked on " + selectedItemIndex);
            }
        });
        if (notificationList.getItems().isEmpty()) {
            hideAddPersonPane();
        } else {
            showAddPersonPane();
        }

        nameTextField.setDisable(true);

    }

    //This function is called to the start of the application
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            init();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //JUST FOR TESTING
    public void fillGridPane() {
        fillGridPane( clusteredImgDescriptor );
    }

    public void fillGridPane( List<ImgDescriptor> list ) {
        if (list == null || list.isEmpty()) {
            return;
        }

        List<String> imageIds = new ArrayList<>();
        for (ImgDescriptor descriptor : list) {
            imageIds.add( descriptor.getId() );
        }
        List<File> picsFile = null;
        try {
            picsFile = unknownPeopleMongoDB.downloadImages(imageIds);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        photoPane.getChildren().clear();//Delete all the pane in the grid

        int numCols = Parameters.GRID_COLUMN_NUMBER;
        int numRows = (int)Math.floor(list.size()/Parameters.GRID_COLUMN_NUMBER);

        for (int i = 0; i < numCols; i++) {
            ColumnConstraints colConstraints = new ColumnConstraints();
            colConstraints.setHgrow(Priority.SOMETIMES);
            photoPane.getColumnConstraints().add(colConstraints);
        }

        for (int i = 0; i < numRows; i++) {
            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setVgrow(Priority.SOMETIMES);
            photoPane.getRowConstraints().add(rowConstraints);
        }

        // Initialize matrix dynamically
        imgDescMatrix = new ImgDescriptor[numRows][numCols];
        
        Iterator it = list.iterator();
        int index = 0;
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; picsFile != null && j < numCols && index < picsFile.size(); j++) {
                File file = picsFile.get(index++);
                ImgDescriptor imgD = (ImgDescriptor) it.next();
                String id = imgD.getId();
                
                Image img = new Image( file.toURI().toString(), Parameters.GRID_IMG_HEIGHT, Parameters.GRID_IMG_WIDTH, false, false);

                addPane( new ImageView(img), i, j);
                imgDescMatrix[i][j] = new ImgDescriptor( imgD );
                //System.out.println("Inserted in the grid pane: " + imgD);
            }
        }
    }

    /**
     * Add a new pane to the gridPane containing a new image
     */
    private void addPane(ImageView img, int rowIndex, int colIndex) {
        Pane pane = new Pane(img);
        pane.setStyle( Parameters.UNSELECTED_PANE_STYLE );
        
        pane.setOnMouseClicked(e -> {
            //rowIndex and RowIndex are the correct indexes of the selected item

            if (selectedItemList.contains(imgDescMatrix[rowIndex][colIndex])) {
                unselectPane(pane);
                selectedPaneList.remove(pane);
                selectedItemList.remove(imgDescMatrix[rowIndex][colIndex]);
                System.out.println("Unselected item");
            } else {
                if (selectedItemList.size() > (Parameters.MAX_SELECTABLE_PHOTOS -1) ) {
                    emitAlert(Parameters.MAX_SELECTED_PHOTOS_MESSAGE);
                    return;
                }
                selectPane(pane);
                selectedPaneList.add(pane);
                selectedItemList.add(imgDescMatrix[rowIndex][colIndex]);
                System.out.println("Selected item");
            }
            System.out.printf("Mouse clicked cell [%d, %d]%n", rowIndex, colIndex);

        });

        photoPane.add(pane, colIndex, rowIndex);//Want the parameter inverted       
    }

    private void handleAddButton( ) {
        personToBeAuthorizedName = nameTextField.getText();
        System.out.println("Inserted this name: " + personToBeAuthorizedName);
        if (personToBeAuthorizedName.equals("")) {
            emitAlert( Parameters.EMPTY_NAME_MESSAGE);
            return;
        } else if( selectedItemList.size() != Parameters.MAX_SELECTABLE_PHOTOS ) {
            emitAlert( Parameters.NOT_ENOUGH_SELECTED_PHOTOS_MESSAGE );
            return;
        }

        //selectedItemList.size() == Parameters.MAX_SELECTABLE_PHOTOS
        checkPresence();
    }

    private void handleClearButton() {
        notificationList.getItems().clear();
        System.out.println( "Alert list cleared");

        selectedItemIndex = -1;
        itemSelected = false;
        
        hideAddPersonPane();
        photoPane.getChildren().clear();
    }
    
    private void handleDeleteButton() {
        removeSelectedItem( notificationList );
    }
    
    private void handleClearSelectionButton() {
        clearSelection();
    }

    private void handleRemoveButton() {
        if(selectedItemIndex == -1)
            return;
        
        emitAlert( Parameters.REMOVING_AUTHORIZED_PERSON_CONFIRMATION_MESSAGE );
    }
    
    private void handleDeleteAllUnknownButton() {
         emitAlert( Parameters.REMOVING_ALL_UNKNOWN_CONFIRMATION_MESSAGE );
    }
    
    private void emitAlert( String message ) {
        Alert alert = null;
        if (message.equals(Parameters.PERSON_ALREADY_EXISTING_MESSAGE)) {
            alert = new Alert(AlertType.WARNING, message, ButtonType.NO, ButtonType.YES);
        } else if (message.equals(Parameters.AUTHORIZATION_ALERT_MESSAGE)) {
            alert = new Alert(AlertType.WARNING, message, ButtonType.NO, ButtonType.YES);
        } else if (message.equals(Parameters.AUTHORIZATION_CONFIRMATION_MESSAGE)) {
            alert = new Alert(AlertType.CONFIRMATION, message, ButtonType.NO, ButtonType.YES );
        } else if (message.equals(Parameters.MAX_SELECTED_PHOTOS_MESSAGE)) {
            alert = new Alert(AlertType.WARNING, message, ButtonType.OK);
        } else if (message.equals(Parameters.NOT_ENOUGH_SELECTED_PHOTOS_MESSAGE)) {
            alert = new Alert(AlertType.WARNING, message, ButtonType.OK);
        } else if (message.equals(Parameters.EMPTY_NAME_MESSAGE)) {
            alert = new Alert(AlertType.ERROR, message, ButtonType.OK);
        } else if (message.equals(Parameters.REMOVING_AUTHORIZED_PERSON_CONFIRMATION_MESSAGE))
            alert = new Alert(AlertType.CONFIRMATION, message, ButtonType.NO, ButtonType.YES);
        else if (message.equals(Parameters.REMOVING_ALL_UNKNOWN_CONFIRMATION_MESSAGE))
            alert = new Alert(AlertType.CONFIRMATION, message, ButtonType.NO, ButtonType.YES);

        alert.showAndWait();

        performActionOnAlertResponse( message, alert.getResult() );
    }

    private void performActionOnAlertResponse( String message, ButtonType result) {
        if( message.equals(Parameters.AUTHORIZATION_ALERT_MESSAGE) ) {
            if( result.equals( ButtonType.YES ) ) {
                System.out.println("SELECTED: YES");
                retrieveNewPersonImages();
            }
        } else if (message.equals(Parameters.AUTHORIZATION_CONFIRMATION_MESSAGE) ) {
            if( result.equals( ButtonType.YES ) ) {
                System.out.println("SELECTED: YES");
                recordAuthorizedPerson();
            }
        }  else if ( message.equals(Parameters.PERSON_ALREADY_EXISTING_MESSAGE)) {
            if( result.equals( ButtonType.YES ) ) {
                System.out.println("SELECTED: YES");

                // Remove authorized person before adding 
                removeAuthorizedPersonFromName( personToBeAuthorizedName );
                
                recordAuthorizedPerson();   
            }
        } else if( message.equals(Parameters.REMOVING_AUTHORIZED_PERSON_CONFIRMATION_MESSAGE) ){
            if( result.equals( ButtonType.YES ) )
                removeAuthorizedPerson();
        } else if( message.equals(Parameters.REMOVING_ALL_UNKNOWN_CONFIRMATION_MESSAGE) )
            if( result.equals( ButtonType.YES ) )
                removeAllUnknownPeople();
    }
    
    private void checkPresence( ) {
        //First search the name in the DB
        System.out.println( "Check the presence of " + personToBeAuthorizedName + " in the DB" );
        boolean present = authorizedPeopleMongoDB.existsUser( personToBeAuthorizedName );
        if( present ) {
            System.out.println("Already present!");
            emitAlert(Parameters.PERSON_ALREADY_EXISTING_MESSAGE);
        } else {
            System.out.println("Not present yet!");
            emitAlert(Parameters.AUTHORIZATION_CONFIRMATION_MESSAGE);
        }
    }

    private void recordAuthorizedPerson( ) {
        //The folder does not exists surely!        
        addAuthorizedPerson();
        
        for( ImgDescriptor imgD : selectedItemList )
            unknownPeopleMongoDB.removeUnknownPeople( imgD.getLabel() );
        
        selectedItemList.clear();
        removeSelectedItem( notificationList );
        
        hideAddPersonPane();
        
        //Update the pane
        photoPane.getChildren().clear();
        
        //Remove also the other images related to him
        removeRelatedImages( personToBeAuthorizedName );
        
        //Re-execute of the clustering
        clusteringRunnable.run();
    }
    
    //Add the person to the list and also to the DB
    private void addAuthorizedPerson() {
        authorizedPeopleMongoDB.createAuthorizedPerson(personToBeAuthorizedName, selectedItemList);
        if( authorizedPeople.contains( personToBeAuthorizedName ) == false) {
            authorizedPeopleList.getItems().add( personToBeAuthorizedName );
            authorizedPeople.add( personToBeAuthorizedName );
        }
    }
    
    //Remove alse all the image classified as the new added person
    private void removeRelatedImages( String name ) {
        KNNClassifier knnClassifier = null;
        try {
            knnClassifier = new KNNClassifier();
        }catch(Exception e ){
            e.printStackTrace();
        }
        
        List<ImgDescriptor> copyList = new ArrayList<>( clusteredImgDescriptor );
        System.out.println( "Delete all related images. Dim Unknwon: " + copyList.size() );
        int removed = 0;
        for( ImgDescriptor imgD : copyList ) {

            PredictedClass predClass = knnClassifier.predict( imgD );
            if( predClass.getConfidence() > Parameters.KNN_CONF_THRESHOLD ) { //Filter
                System.out.println( "Predicted class: " + predClass.getLabel()+"\n\n" );
                if( predClass.getLabel().equals( name ) ) {
                    System.out.println( "Remove related image" );
                    unknownPeopleMongoDB.removeUnknownPeople( imgD.getLabel() );
                    removed++;
                }
            }
        }
        System.out.println( "Removed related images: " + removed );
        
    }
    
    //Remove the person from the list and also from the DB
    private void removeAuthorizedPerson() {
        if( selectedItemIndex == -1 )
            return;
        
        System.out.println( "\nElem to be elem: " + authorizedPeople.get( selectedItemIndex ) + "\n\n" );
        
        authorizedPeopleMongoDB.removeAuthorizedPerson( authorizedPeople.get( selectedItemIndex ) );
        
        removeSelectedItem( authorizedPeopleList );
        
        System.out.println( "\n\nList of auth person POST DELETE from INDEX:\n" );
        for(String s : authorizedPeopleMongoDB.listAuthorizedPeopleName() )
            System.out.println( s );
        
        System.out.println( "\n\n" );
    }
    
    //Remove the person from the list and also from the DB
    private void removeAuthorizedPersonFromName( String name ) {
        if( name == null || name.equals("") )
            return;
                
        int index = authorizedPeople.indexOf( name );
        authorizedPeopleMongoDB.removeAuthorizedPerson( name );
        authorizedPeopleList.getItems().remove(index);
        authorizedPeople.remove( name );
        authorizedPeopleList.getSelectionModel().clearSelection();
        
        System.out.println( "\n\nList of auth person POST DELETE from name:\n" );
        for(String s : authorizedPeopleMongoDB.listAuthorizedPeopleName() )
            System.out.println( s );
        
        System.out.println( "\n\n" );
    }

    private void removeAllUnknownPeople() {
        if( clusteredImgDescriptor == null || clusteredImgDescriptor.isEmpty() )
            return;
        
        System.out.println( "\nStopping the sheduled thread before\n" );
        scheduleAtFixedRate.cancel(true);
        
        for(ImgDescriptor imgD : clusteredImgDescriptor ) {
            unknownPeopleMongoDB.removeUnknownPeople( imgD.getLabel() );
        }
        
        //Remove all notification
        selectedItemIndex = -1;
        notificationList.getItems().clear();
        selectedItemList.clear();
        hideAddPersonPane();
        photoPane.getChildren().clear();
        
        System.out.println( "Restart of the scheduler" );
        scheduleAtFixedRate = scheduler.scheduleAtFixedRate(clusteringRunnable, 0, Parameters.PERIODIC_TIME_AMOUNT, Parameters.PERIODIC_TIME_TYPE);
    }
    
    private void retrieveNewPersonImages() {
        try {
            fillGridPane();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void selectPane(Pane pane) {
        if (selectedPaneList.contains(pane) == false)
            pane.setStyle( Parameters.SELECTED_PANE_STYLE);
        
    }

    private void unselectPane(Pane pane) {
        if (selectedPaneList.contains( pane ) )
            pane.setStyle( Parameters.UNSELECTED_PANE_STYLE );
        
    }

    private void hideAddPersonPane() {
        nameTextField.setVisible(false);
        nameTextField.clear();
        addButton.setVisible(false);
        personLabel.setVisible(false);
        deleteButton.setVisible(false);
        clearButton.setVisible( false );
        clearSelectionButton.setVisible(false);
    }

    private void showAddPersonPane() {
        nameTextField.setVisible(true);
        addButton.setVisible(true);
        personLabel.setVisible(true);
        deleteButton.setVisible(true);
        clearButton.setVisible( true );
        clearSelectionButton.setVisible(true);
    }
    
    private void removeSelectedItem( ListView list ) {
        System.out.println("Remove selected item for list: " + list.getId() );

        if( selectedItemIndex == -1 ) 
            return;
        
        if( list.equals( notificationList ) ){
            notificationList.getItems().remove(selectedItemIndex);
            notificationList.getSelectionModel().clearSelection();
            System.out.println( "Delete item " + selectedItemIndex + " from the alert list");
            
            if( selectedItemList.isEmpty() )
                hideAddPersonPane();
        } else if( list.equals( authorizedPeopleList ) ) {
            authorizedPeopleList.getItems().remove(selectedItemIndex);
            authorizedPeopleList.getSelectionModel().clearSelection();
            
            authorizedPeople.remove( selectedItemIndex );
            System.out.println( "Delete item " + selectedItemIndex + " from the Authorized list");
        }
        
        selectedItemIndex = -1;
        itemSelected = false;
    }

    private void clearSelection() {
        for (Pane pane : selectedPaneList)
            unselectPane(pane);

        selectedPaneList.clear();

        selectedItemList.clear();
    }

}
