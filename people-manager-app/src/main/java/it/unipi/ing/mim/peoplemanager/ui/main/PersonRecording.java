package it.unipi.ing.mim.peoplemanager.ui.main;

import it.unipi.ing.mim.peoplemanager.ui.db.UnknownPeopleMongoDB;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class PersonRecording extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        stage.setTitle("Face Recording");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);
        UnknownPeopleMongoDB unknownPeopleMongo = new UnknownPeopleMongoDB();
        if (!unknownPeopleMongo.isCollectionExists()){
            unknownPeopleMongo.rebuildUnknownCollection();
        }        
        launch(args);
    }
    
}
