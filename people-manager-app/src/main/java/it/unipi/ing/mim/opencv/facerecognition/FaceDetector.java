package it.unipi.ing.mim.opencv.facerecognition;

import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;

public class FaceDetector {

	private CascadeClassifier face_cascade;

	//TODO
	public FaceDetector(String haarcascadePath) {
		//init detector
		face_cascade = new CascadeClassifier( haarcascadePath );
	}

	//TODO
	/**
	 * using detectMultiScale and creating a new instance of class RectVector to be passed as return value
	 * detectMultiScale(img, face, sf, minN, flags, minS, maxS);
	 * @param img
	 * @param minSize
	 * @param maxSize
	 * @return
	 */
	public RectVector detect(Mat img, Size minSize, Size maxSize) {
		//detect faces
		RectVector faces = new RectVector();
		
		face_cascade.detectMultiScale( img, faces, 1.05, 3, opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING, minSize, maxSize);
		
		return faces;
	}
}
