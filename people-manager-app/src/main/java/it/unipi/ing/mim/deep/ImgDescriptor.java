package it.unipi.ing.mim.deep;

import java.io.Serializable;

public class ImgDescriptor implements Serializable, Comparable<ImgDescriptor> {

    private static final long serialVersionUID = 1L;

    private float[] normalizedVector; // image feature

    private String id; // unique id of the image (usually file name)

    private String label;

    private double dist; // used for sorting purposes

    private double weight; // used for sorting purposes
    
    private Integer clusterNumber;

    public Integer getClusterNumber() {
        return clusterNumber;
    }

    public void setClusterNumber(Integer clusterNumber) {
        this.clusterNumber = clusterNumber;
    }

    public ImgDescriptor(float[] features, String id, String label) {
        if (features != null) {
            float norm2 = evaluateNorm2(features);
            this.normalizedVector = getNormalizedVector(features, norm2);
        }
        this.id = id;
        this.label = label;
    }

    public ImgDescriptor( ImgDescriptor imgD ) {
        this.normalizedVector = imgD.normalizedVector; // image feature

        this.id = imgD.id; // unique id of the image (usually file name)

        this.label = imgD.label;

        this.dist = imgD.dist; // used for sorting purposes

        this.weight = imgD.weight; // used for sorting purposes

        this.clusterNumber = imgD.clusterNumber;
    
    }
    public float[] getFeatures() {
        return normalizedVector;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void computeWeight(ImgDescriptor desc) {
        double distance = distance(desc);
        if (distance != 0){
            this.weight = 1.0 / Math.pow(distance, 2);
        }else{
            this.weight = Double.MAX_VALUE;
        }
    }

    // compare with other friends using distances
    @Override
    public int compareTo(ImgDescriptor arg0) {
        return new Double(dist).compareTo(arg0.dist);
    }

    //evaluate Euclidian distance
    public double distance(ImgDescriptor desc) {
        float[] queryVector = desc.getFeatures();

        dist = 0;
        for (int i = 0; i < queryVector.length; i++) {
            dist += (normalizedVector[i] - queryVector[i]) * (normalizedVector[i] - queryVector[i]);
        }
        dist = Math.sqrt(dist);

        return dist;
    }

    //Normalize the vector values 
    private float[] getNormalizedVector(float[] vector, float norm) {
        if (norm != 0) {
            for (int i = 0; i < vector.length; i++) {
                vector[i] = vector[i] / norm;
            }
        }
        return vector;
    }

    //Norm 2
    private float evaluateNorm2(float[] vector) {
        float norm2 = 0;
        for (int i = 0; i < vector.length; i++) {
            norm2 += (vector[i]) * (vector[i]);
        }
        norm2 = (float) Math.sqrt(norm2);

        return norm2;
    }

    @Override
    public String toString() {
        return "ImgDescriptor{" + "id=" + id + ", label=" + label + ", dist=" + dist + ", weight=" + weight + ", clusterNumber=" + clusterNumber + ", normalizedVector=" + normalizedVector + '}';
    }
}
