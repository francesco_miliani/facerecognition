package it.unipi.ing.mim.opencv.facerecognition.ui.component;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {

    private BufferedImage image;

    public ImagePanel() {
    }
    
    public void setImage(BufferedImage image){
        this.image = image;
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if (image != null){
            g.drawImage(image, 0, 0, this); // see javadoc for more info on the parameters            
        }        
    }

}
