package it.unipi.ing.mim.opencv.facerecognition.thread;

import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.opencv.facerecognition.data.UnknownPersonData;
import it.unipi.ing.mim.peoplemanager.db.UnknownPeopleMongo;
import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

public class UnknownRegisterThread extends Thread {

    public LinkedBlockingQueue<UnknownPersonData> queueUnknownPeople = new LinkedBlockingQueue<UnknownPersonData>();
    private boolean existsUnknownDirectory;

    public UnknownRegisterThread() {
    }

    public void addUnknownPerson(UnknownPersonData personData) {
        try {
            queueUnknownPeople.put(personData);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        try {
            UnknownPersonData personData = null;
            UnknownPeopleMongo unknownPeopleMongo = new UnknownPeopleMongo();
            while ((personData = queueUnknownPeople.take()) != null) {
                String name = personData.getName() + ".jpg";
                File tempFile = File.createTempFile("unk", ".jpg");
                String dest = tempFile.getAbsolutePath();
                imwrite(dest, personData.getMat());
                unknownPeopleMongo.createUnknownPerson(name, personData.getFeatures(), tempFile);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
