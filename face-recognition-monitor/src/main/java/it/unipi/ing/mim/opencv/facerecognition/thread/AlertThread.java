/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.ing.mim.opencv.facerecognition.thread;

import java.util.concurrent.LinkedBlockingQueue;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 *
 * @author n3gr3t4
 */
public class AlertThread extends Thread {

    public LinkedBlockingQueue<String> alerts = new LinkedBlockingQueue<String>();

    private static final String UNKOWN_ALERT = "UNKOWN_Person";
    private boolean mute;

    public boolean isMute() {
        return mute;
    }

    public void mute() {
        mute = true;
    }

    public void unMute() {
        mute = false;
    }

    public void emitAlert() {
        try {
            alerts.put(UNKOWN_ALERT);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        String alertType = null;
        try {
            while ((alertType = alerts.take()) != null) {
                try {
                    if (!mute && alertType.equalsIgnoreCase(UNKOWN_ALERT)) {
                        AudioInputStream audioIn = AudioSystem.getAudioInputStream(AlertThread.class.getResource("/sounds/beep-02.wav"));
                        Clip clip = AudioSystem.getClip();
                        clip.open(audioIn);
                        clip.start();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
