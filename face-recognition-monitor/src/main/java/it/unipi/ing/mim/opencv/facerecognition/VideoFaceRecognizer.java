package it.unipi.ing.mim.opencv.facerecognition;

import it.unipi.ing.mim.deep.DNNExtractor;
import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.opencv.facerecognition.data.UnknownPersonData;
import it.unipi.ing.mim.opencv.facerecognition.tools.BoundingBox;
import it.unipi.ing.mim.opencv.facerecognition.ui.FaceDetectionJFrame;
import it.unipi.ing.mim.opencv.facerecognition.ui.component.ImagePanel;
import java.awt.Color;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bytedeco.javacpp.opencv_core;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameUtils;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.javacv.OpenCVFrameGrabber;

public class VideoFaceRecognizer extends Thread {

    private FaceDetector faceDetector;

    private KNNClassifier knnClassifier;

    private OpenCVFrameGrabber openCVframeGrabber = null;

    private OpenCVFrameConverter.ToMat frame2Mat;

    private DNNExtractor extractor;

    private static final int CAMERA_SRC = 0;

    private FaceDetectionJFrame faceDetectionJFrame;
    private ImagePanel imagePanel;

    private Map<String, Integer> identifiedPeopleMap = new HashMap<String, Integer>();
    
    private transient boolean start;

    //Modificare il nome della classe per fare il grabber
    public VideoFaceRecognizer(String haarcascadePath, File storageFile) throws ClassNotFoundException, IOException {
        extractor = DNNExtractor.getInstance();
        faceDetector = new FaceDetector(haarcascadePath);
        knnClassifier = new KNNClassifier();
        frame2Mat = new OpenCVFrameConverter.ToMat();
    }

    public VideoFaceRecognizer(FaceDetectionJFrame faceDetectionJFrame, String haarcascadePath, File storageFile) throws ClassNotFoundException, IOException {
        this(haarcascadePath, storageFile);
        this.faceDetectionJFrame = faceDetectionJFrame;
        this.imagePanel = faceDetectionJFrame.getImagePanel();
    }

    public void initVideo(File srcVideo, File destVideoFile) throws Exception {
        openCVframeGrabber = OpenCVFrameGrabber.createDefault(CAMERA_SRC);
        openCVframeGrabber.setImageHeight(imagePanel.getHeight());
        openCVframeGrabber.setImageWidth(imagePanel.getWidth());
    }

    public void updateCameraSize(int width, int height) throws Exception {
        openCVframeGrabber.setImageHeight(height);
        openCVframeGrabber.setImageWidth(width);
    }
    
    public void start(){
        super.start();
        start = true;
    }

    //TODO
    public void run() {
        try {
            openCVframeGrabber.start();
            List<String> identifiedPeopleInFrame = new ArrayList<String>();
            Frame frame = null;
            BufferedImage bi = null;
            while (start && (frame = openCVframeGrabber.grab()) != null) {//DECOMMENT THIS ROW TO ENABLE THE RECOGNITION ON THE VIDEOCAMERA
                identifiedPeopleInFrame.clear();
                //while ((frame = grabber.grab()) != null) {//DECOMMENT THIS ROW TO ENABLE THE RECOGNITION ON THE VIDEO
                //Detect all the faces in the frame and classify them
                //highlight them with a bounding box and the classification label using BoundingBox.highlight method
                // 1) FACE DETECTION
                // 2) BOUNDING BOX
                // 3) KNN CLASSIFIER
                // 4) LABEL THE BOUNDING BO
                Mat img = frame2Mat.convert(frame);
                RectVector faces = faceDetector.detect(img, Parameters.FACE_MIN_SIZE, Parameters.FACE_MAX_SIZE);
                if (!faces.empty()) {
                    for (int i = 0; i < faces.size(); i++) {
                        Mat regioneOfInterest = BoundingBox.getImageROI(img, faces.get(i));//From the image I take only the region of interest
                        float[] features = extractor.extract(regioneOfInterest, Parameters.DEEP_LAYER);
                        ImgDescriptor query = new ImgDescriptor(features);
                        PredictedClass predClass = knnClassifier.predict(query);
                        if (predClass.getConfidence() > Parameters.KNN_CONF_THRESHOLD) { //Filter
                            BoundingBox.highlight(img, faces.get(i), predClass.getLabel() + "  " + String.format("%.2f", (predClass.getConfidence() * 100)) + "%", Scalar.GREEN);
                            identifiedPeopleInFrame.add(predClass.getLabel());
                        } else //I do NOT know who he is!
                        {
                            BoundingBox.highlight(img, faces.get(i), "UNKNOWN", Scalar.RED);
                            identifiedPeopleInFrame.add("UNKNOWN");
                            /* COMMENTED THIS LINE, OTHERWISE IT WILL STORE MANY FACES INTO THE UNKNOWN DIRECTORY */
                            storeUnknownImageFrame(regioneOfInterest, features);
                        }
                        //after identified a person it is required to update the image on the JPanel
                        bi = Java2DFrameUtils.toBufferedImage(frame);
                        imagePanel.setImage(bi);
                        imagePanel.invalidate();
                        imagePanel.repaint();
                    }
                } else {
                    //in case there are no person identified, it is required to update the image on the JPanel
                    //here the image displayed in the JPanel moves fast as in the video
                    bi = Java2DFrameUtils.toBufferedImage(frame);
                    imagePanel.setImage(bi);
                    imagePanel.invalidate();
                    imagePanel.repaint();
                }
                updateTable(identifiedPeopleInFrame);
            }//ehd while
        } catch (FrameGrabber.Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateTable(List<String> identifiedPeopleInFrame) {
        if (!identifiedPeopleInFrame.isEmpty()) {
            for (String person : identifiedPeopleInFrame) {
                if (!identifiedPeopleMap.containsKey(person)) {
                    identifiedPeopleMap.put(person, Parameters.MAX_NUMBER_DISAPPEARING_FRAMES);
                    faceDetectionJFrame.addAccessEntry(person);
                    if ("UNKNOWN".equalsIgnoreCase(person)){
                        faceDetectionJFrame.emitAlert();
                    }
                }
            }
            Set<String> keysMap = identifiedPeopleMap.keySet();
            List<String> keysToRemove = new ArrayList<String>();
            for (String entryMapKey : keysMap) {
                if (!identifiedPeopleInFrame.contains(entryMapKey)) {
                    int updatedCount = identifiedPeopleMap.get(entryMapKey) - 1;
                    if (updatedCount == 0) {
                        keysToRemove.add(entryMapKey);
                    } else {
                        identifiedPeopleMap.put(entryMapKey, updatedCount);
                    }
                }
            }
            if (!keysToRemove.isEmpty()) {
                for (String keyToRemove : keysToRemove) {
                    identifiedPeopleMap.remove(keyToRemove);
                }
            }

        }
    }

    private void storeUnknownImageFrame(Mat mat, float[] features) {
        String id = System.currentTimeMillis() + "";
        UnknownPersonData personData = new UnknownPersonData(id, features, mat);
        //Record 1/3 image
        if( Math.random()*3 > 2 )
            faceDetectionJFrame.addUnknownPerson(personData);
    }

}
