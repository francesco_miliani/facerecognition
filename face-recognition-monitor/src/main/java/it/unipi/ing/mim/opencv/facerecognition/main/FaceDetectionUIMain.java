/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.ing.mim.opencv.facerecognition.main;

import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.windows.WindowsLookAndFeel;
import it.unipi.ing.mim.opencv.facerecognition.ui.FaceDetectionJFrame;
import it.unipi.ing.mim.peoplemanager.db.AuthorizedPeopleMongoDB;
import java.util.logging.Level;
import javax.swing.UIManager;

public class FaceDetectionUIMain {

    public static void main(String[] args) {
        java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);
        AuthorizedPeopleMongoDB authorizedPeopleMongoDB = new AuthorizedPeopleMongoDB();
        if (!authorizedPeopleMongoDB.isCollectionExists()){
            authorizedPeopleMongoDB.rebuildAuthorizedCollection();
        }
        try {
            UIManager.setLookAndFeel(System.getProperty("os.name").toLowerCase().startsWith("windows")
                    ? new WindowsLookAndFeel() : new PlasticXPLookAndFeel());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        FaceDetectionJFrame faceDetectionJFrame = new FaceDetectionJFrame();
        faceDetectionJFrame.setVisible(true);
        faceDetectionJFrame.init();
    }

}
