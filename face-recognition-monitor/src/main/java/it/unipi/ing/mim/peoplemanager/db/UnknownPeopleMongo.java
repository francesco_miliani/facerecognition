package it.unipi.ing.mim.peoplemanager.db;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import it.unipi.ing.mim.deep.DNNExtractor;
import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.bson.Document;
import org.bson.types.ObjectId;

public class UnknownPeopleMongo {

    private static final String MONGODB_SERVER = "localhost";
    private static final Integer MONGODB_PORT = 27017;
    private static final String MONGODB_DATABASE = "facerecognition";
    private static final String UNKNOWN_PEOPLE_COLLECTION = "unknown_people";

    private static final String UNKNOWN_PEOPLE_PICS_COLLECTION = "unknown_people_pics";
    
    private MongoClient connectToMongoDB() {
        return new MongoClient(MONGODB_SERVER, MONGODB_PORT);
    }
    
    public boolean isCollectionExists() {
        MongoClient mongoClient = connectToMongoDB();
        MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
        boolean exists = isCollectionExists(database);
        mongoClient.close();
        return exists;
    }
    
    private boolean isCollectionExists(MongoDatabase database) {
        MongoIterable<String> collection = database.listCollectionNames();
        for (String s : collection) {
            if (s.equals(UNKNOWN_PEOPLE_COLLECTION)) {
                return true;
            }
        }
        return false;
    }    

    public void rebuildUnknownCollection() throws FileNotFoundException {
        // Delete All documents from collection Using blank BasicDBObject
        MongoClient mongoClient = connectToMongoDB();
        MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
        if (isCollectionExists(database)) {
            database.getCollection(UNKNOWN_PEOPLE_COLLECTION).drop();
            database.getCollection(UNKNOWN_PEOPLE_PICS_COLLECTION + ".files").drop();
            database.getCollection(UNKNOWN_PEOPLE_PICS_COLLECTION + ".chunks").drop();
        }
        mongoClient.close();
        File unknownFolder = Parameters.UNKNOWN_FOLDER;
        File[] imgFiles = unknownFolder.listFiles();
        for (File imgFile : imgFiles) {
            String imageName = imgFile.getName();
            if (!imageName.startsWith(".")) {
                float[] featureArr = null;
                try {
                    featureArr = DNNExtractor.getInstance().extract(imgFile, Parameters.DEEP_LAYER);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                imageName = System.currentTimeMillis() + ".jpg";
                createUnknownPerson(imageName, featureArr, imgFile);
            }
        }
    }

    public void createUnknownPerson(String imageName, float[] features, File file) throws FileNotFoundException {
        MongoClient mongoClient = connectToMongoDB();
        MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
        MongoCollection<Document> unknownPeopleCollection = database.getCollection(UNKNOWN_PEOPLE_COLLECTION);

        GridFSBucket gridFSBucket = GridFSBuckets.create(database, UNKNOWN_PEOPLE_PICS_COLLECTION);
        GridFSUploadOptions options = new GridFSUploadOptions().chunkSizeBytes(1024);
        options.metadata(new Document());
        options.getMetadata().append("content_type", "image/jpg");
        options.getMetadata().append("type", "image");
        ObjectId fileId = gridFSBucket.uploadFromStream(imageName, new FileInputStream(file), options);
    //.metadata(new Document("type", "image").append("upload_date", format.parse("2016-09-01T00:00:00Z")).append("content_type", "image/jpg"));
        Document document = new Document("filename", imageName);
        document.append("file_id", fileId);
        document.append("update_date", new Date());
        BasicDBList list = new BasicDBList();
        for (Float feature : features) {
            list.add(feature);
        }
        document.append("features", list);

        unknownPeopleCollection.insertOne(document);
        mongoClient.close();
    }

    public List<ImgDescriptor> listAllUnknownPeople() {
        List<ImgDescriptor> listDesc = new ArrayList<ImgDescriptor>();
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            MongoCollection<Document> unknownPeopleCollection = database.getCollection(UNKNOWN_PEOPLE_COLLECTION);
            FindIterable<Document> cursor = unknownPeopleCollection.find();
            listDesc = buildListImgDescriptor(cursor);
        }
        return listDesc;
    }
    
    public List<ImgDescriptor> listAllUnknownPeopleFrom(Date date) {
        List<ImgDescriptor> listDesc = new ArrayList<ImgDescriptor>();
        try (MongoClient mongoClient = connectToMongoDB()) {
            MongoDatabase database = mongoClient.getDatabase(MONGODB_DATABASE);
            MongoCollection<Document> unknownPeopleCollection = database.getCollection(UNKNOWN_PEOPLE_COLLECTION);
            BasicDBObject query = new BasicDBObject();
            query.put("update_date", BasicDBObjectBuilder.start("$gte", date).get());            
            FindIterable<Document> cursor = unknownPeopleCollection.find(query);
            listDesc = buildListImgDescriptor(cursor);
        }
        return listDesc;
    }    

    private List<ImgDescriptor> buildListImgDescriptor(FindIterable<Document> cursor) {
        List<ImgDescriptor> listDesc = new ArrayList<>();
        Iterator<Document> it = cursor.iterator();
        while (it.hasNext()) {
            Document dbObject = it.next();
            String filename = dbObject.getString("filename");
            ArrayList<Double> features = (ArrayList<Double>) dbObject.get("features");
            float[] desc_features = new float[features.size()];
            for (int i = 0; i < features.size(); i++) {
                desc_features[i] = features.get(i).floatValue();
            }
            ImgDescriptor desc = new ImgDescriptor(desc_features, filename, filename);
            listDesc.add(desc);
        }
        return listDesc;
    }

    public static void main(String[] args) throws Exception {
        UnknownPeopleMongo unknownPeopleMongo = new UnknownPeopleMongo();
        unknownPeopleMongo.rebuildUnknownCollection();
        List<ImgDescriptor> descriptors = unknownPeopleMongo.listAllUnknownPeople();
        for (ImgDescriptor imgDescriptor : descriptors) {
            System.out.println(imgDescriptor.getLabel() + " " + imgDescriptor.getId());
        }
        // since this date is recent and there is no new date, it is expected 0 rows.
        descriptors = unknownPeopleMongo.listAllUnknownPeopleFrom(new Date());
        System.out.println("descriptors " + descriptors.size());

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -1);
        // since this date is ONE hour ago, it is expected to get many rows.
        descriptors = unknownPeopleMongo.listAllUnknownPeopleFrom(calendar.getTime());
        System.out.println("descriptors " + descriptors.size());        
    }

}
