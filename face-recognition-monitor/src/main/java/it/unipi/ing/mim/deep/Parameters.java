package it.unipi.ing.mim.deep;

import java.io.File;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.bytedeco.javacpp.opencv_core.Size;

public class Parameters {

    //DEEP parameters
    public static final String DEEP_PROTO = "data/caffe/resnet50_ft.prototxt";
    public static final String DEEP_MODEL = "data/caffe/resnet50_ft.caffemodel";

    public static final String DEEP_LAYER = "pool5/7x7_s1";
    public static final int IMG_WIDTH = 224;
    public static final int IMG_HEIGHT = 224;
    //Image Source Folder
    public static final File SRC_FOLDER = new File("data/classes");
    public static final File UNKNOWN_FOLDER = new File("data/unknown");

    //Features Storage File
    public static final File STORAGE_FILE = new File("data/features.dat");

    //k-Nearest Neighbors
    public static final int K = 10;

    public static final int K_CLUSTERING = 50;

    public static final int MAX_NUMBER_DISAPPEARING_FRAMES = 10;

    //HTML Output Parameters
    public static final String BASE_URI = "file:///" + Parameters.SRC_FOLDER.getAbsolutePath() + "/";
    public static final File RESULTS_HTML = new File("out/deep.seq.html");
    public static final File RESULTS_HTML_LUCENE = new File("out/deep.lucene.html");
    public static final File RESULTS_HTML_REORDERED = new File("out/deep.reordered.html");

    //Face Detection Parameters 
    public static final Size FACE_MIN_SIZE = new Size(120, 120);
    public static final Size FACE_MAX_SIZE = new Size(500, 500);
    public static final Size EYE_MIN_SIZE = new Size(40, 40);
    public static final Size EYE_MAX_SIZE = new Size(80, 80);
    public static final Size MOUTH_MIN_SIZE = new Size(40, 40);
    public static final Size MOUTH_MAX_SIZE = new Size(200, 70);
    public static final Size NOSE_MIN_SIZE = new Size(20, 20);
    public static final Size NOSE_MAX_SIZE = new Size(60, 60);
    public static final String HAAR_CASCADE_FRONTALFACE = "data/haarcascades/haarcascade_frontalface_alt.xml";
    public static final String HAAR_CASCADE_EYES = "data/haarcascades/haarcascade_eye.xml";
    public static final String HAAR_CASCADE_LEFTEYE = "data/haarcascades/haarcascade_mcs_lefteye.xml";
    public static final String HAAR_CASCADE_RIGHTEYE = "data/haarcascades/haarcascade_mcs_righteye.xml";
    public static final String HAAR_CASCADE_MOUTH = "data/haarcascades/haarcascade_mcs_mouth.xml";
    public static final String HAAR_CASCADE_NOSE = "data/haarcascades/haarcascade_mcs_nose.xml";

    public static final String VIDEO_PATH = "data/videos/dimartedi.mp4";

    public static final int VIDEO_WIDTH = 720;
    public static final int VIDEO_HEIGHT = 576;

    public static final int GRID_IMG_WIDTH = 110;
    public static final int GRID_IMG_HEIGHT = 110;

    public static final int GRID_COLUMN_NUMBER = 5;

    public static final String DATASET_PATH = "data/classes";

    public static final File DEST_VIDEO = new File("out/out.avi");

    public static final float KNN_CONF_THRESHOLD = 0.95f;

    public static final int MAX_SELECTABLE_PHOTOS = 10;

    public static final int ITERATION_NUMBER = 3;

    //Thread data update Config
    public static final int PERIODIC_TIME_AMOUNT = 30;
    public static final TimeUnit PERIODIC_TIME_TYPE = TimeUnit.SECONDS;
    public static final int CALENDAR_TIME_TYPE = Calendar.MINUTE;
    public static final int CALENDAR_TIME_AMOUNT = -5;

    //Messages
    public static final String ALERT_LIST_MESSAGE = "Alert! New unknown person detected at ";
    public static final String ALERT_MESSAGE = "Alert! New unknown person detected. Do you want to authorize it?";
    public static final String CONFIRMATION_MESSAGE = "This person is already granted. Do you want to replace it?";
    public static final String EMPTY_NAME_MESSAGE = "No name typed. Please type the name of the person";
    public static final String MAX_SELECTED_PHOTOS_MESSAGE = "STOP! You can select just " + MAX_SELECTABLE_PHOTOS + " photos";
    public static final String FOLDER_ALREADY_EXISTING_MESSAGE = "Alert! This person already exists! Do you want overwrite it?";
}
