package it.unipi.ing.mim.opencv.facerecognition.thread;

import it.unipi.ing.mim.deep.ImgDescriptor;
import it.unipi.ing.mim.deep.Parameters;
import it.unipi.ing.mim.deep.seq.SeqImageSearch;
import it.unipi.ing.mim.peoplemanager.db.AuthorizedPeopleMongoDB;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class UpdateDataRunnable implements Runnable{

    private AuthorizedPeopleMongoDB authorizedPeopleMongoDB = new AuthorizedPeopleMongoDB();
    private SeqImageSearch seqImageSearch = SeqImageSearch.getInstance();
    
    @Override
    public void run() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Parameters.CALENDAR_TIME_TYPE, Parameters.CALENDAR_TIME_AMOUNT);
        List<String> usersToRemove = authorizedPeopleMongoDB.listDeletedAuthorizedPeople(calendar.getTime());
        if (!usersToRemove.isEmpty()){
            seqImageSearch.removeAuthorizedUsers(usersToRemove);
        }
        Map<String, List<ImgDescriptor>> newAuthorizedUsers = authorizedPeopleMongoDB.listAuthorizedPeopleFrom(calendar.getTime());
        if (!newAuthorizedUsers.isEmpty()){
            seqImageSearch.addAuthorizedUser(newAuthorizedUsers);
        }
    }
    
}
