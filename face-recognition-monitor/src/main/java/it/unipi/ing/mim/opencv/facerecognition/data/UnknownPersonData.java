package it.unipi.ing.mim.opencv.facerecognition.data;

import org.bytedeco.javacpp.opencv_core.Mat;

public class UnknownPersonData {

    private String name;
    private float[] features;
    private Mat mat;

    public UnknownPersonData(String name, float[] features, Mat mat) {
        this.name = name;
        this.features = features;
        this.mat = mat;
    }

    public String getName() {
        return name;
    }

    public Mat getMat() {
        return mat;
    }

    public void setMat(Mat mat) {
        this.mat = mat;
    }
    
    

    public void setName(String name) {
        this.name = name;
    }

    public float[] getFeatures() {
        return features;
    }

    public void setFeatures(float[] features) {
        this.features = features;
    }

}
