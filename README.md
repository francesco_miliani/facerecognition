# Video surveillance application #

## Introduction ##


- to monitor people passing by a specific place, like entrances
- face detection
- record faces to authorize new people
- allow entrance of authorized ones
- reports unauthorized people
- based on the VGG2 Neural Network for feature extraction


Going more in detail, the system consists of 2 applications that communicate through a noSql database, MongoDB, where the information relating to the faces of authorized persons and those identified but not yet controlled are stored.
Periodically, the face-recognition-monitor application extracts data relating to the faces of authorized persons and, when identifying a face, compares the features of this new face with those extracted from the database:
- If they matched, then the identified face would correspond to that of an authorized person
- Otherwise, it would correspond to an unknown person

So, for every unauthorized face, the photos and features are placed in two different collections in the database, which store them
information of unknown faces.

Periodically, the application people-manager-app extracts data related to the faces of unknown people and shows on screen the clusters of unknown faces and the list of the names of authorized persons

- The user can then select 10 pictures of the same person's face and
- authorize it at the entrance
- or clean up the database of unknown faces in case no one should be authorized
- Or replace the photos of an already authorized person, and save the information in the database
- Or, you can revoke a person's authorization, thus deleting the relevant information in the database

The Monitor Application shows the frame sequence captured by the camera:
- Authorized people’s face are highlighted in green
- Unknown people’s faces are highlighted in red
- A table is populated with entries when new faces are detected
- A “beep” is issued when unknown people are detected

When a face is detected:
- The features are extracted and compared to those of the authorized people (extracted from the pictures in the DB)
- A predicted class is associated to the face, with the corresponding confidence, using Weighted K-NN
- If the confidence is higher than a threshold, the prediction is accepted
- Otherwise, the face is considered as belonging to an unknown person
- Faces of unknown people are stored in the DB, for future use by the People Manager app

## People Manager App

His tasks are:

- Allow registration of unknown people
- Allow removal of authorized people

His functioning

1. Periodic K-Means clustering of images related to unknown people
2. Allow an easier selection of those images
3. Alert emitting when a new cluster, related to a specific person, occurs
3. Insertion and removal of authorized people using MongoDB

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact