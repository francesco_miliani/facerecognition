#!/bin/sh

#Install mongodb on mac
echo	"Install mongodb with Homebrew"
brew install mongodb
echo	"Set database's directory"
sudo mkdir -p /data/db

echo	"Start database"
sudo mongod

#Now, you have to create a new terminal and send the following command
echo	"Start database"
sudo mongo --host 127.0.0.1:27017